const mongoose = require("mongoose");

const Product = require("../models/products");


// ------------------ s43 --------------------
// CREATE PRODUCT
module.exports.createProduct = (requestBody, result) => {
	if(result.isAdmin == true){
		let newProduct = new Product({
			name: requestBody.name,
			description: requestBody.description,
			price: requestBody.price,
			stocks: requestBody.stocks
		})
		return newProduct.save().then((newProduct, error) => 
		{
			if(error){
				return error;
			}
			else{
				return newProduct;
			}
		})
	}
	else{
		let message = Promise.resolve('Only Admins are allowed to access this feature.');
		return message.then((value) => {return value});
	}
}


// RETRIEVE ALL ACTIVE PRODUCTS
module.exports.retrieveActiveProducts = () => {
	return Product.find({isActive: true}).then(result => {
		return result;
	})
}







// Find Product by description
module.exports.searchProduct = (request, response) =>{
	let reqBody = request.body.name.toLowerCase();

	let index = 0;

	return Product.find({name: reqBody})
		.then(result => {
			console.log(reqBody[1]);
			response.send(result);
		}
	)
}







// Get all product info
module.exports.getAllProducts = () => {
	return Product.find().then(result => {
		return result;
	})
}


// ------------------ s44 --------------------
// RETRIEVE SINGLE PRODUCT


module.exports.retrieveSingleProduct = (productId) => {
	return Product.findById(productId).then(result => {
		return result;
	})
}

// UPDATE PRODUCT INFORMATION (ADMIN)

module.exports.updateProductInformation = (productId, updatedInfo) => {
	if(updatedInfo.isAdmin == true){
		return Product.findByIdAndUpdate(productId,
			{
				// newData.course.name
				// newData.request.body.name
				name: updatedInfo.product.name, 
				description: updatedInfo.product.description,
				price: updatedInfo.product.price,
				stocks: updatedInfo.product.stocks
			}
		).then((result, error)=>{
			if(error){
				return false;
			}
			return result;
		})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to access this functionality');
		return message.then((value) => {return value});
	}
}



// Archive Product (Admin only)
module.exports.archiveProduct = (productId, archived) => {
	if(archived.isAdmin == true){
		return Product.findByIdAndUpdate(productId,
			{
				isActive: archived.product.isActive
			}
		).then((result, error)=>{
			if(error){
				return false;
			}
			return result;
		})
	}
	else{
		let message = Promise.resolve('No admin rights to access this feature.');
		return message.then((value) => {return value});
	}
}



//  ---------STRETCH GOAL ------------- (Unfinished)
/*
	- Set user as admin (Admin only)
	- Retrieve authenticated user’s orders
	- Retrieve all orders (Admin only)
	- Add to Cart (Highly suggested)
		- Added Products
		- Change product quantities
		- Remove products from cart
		- Subtotal for each item
		- Total price for all items

*/
// Retrieve all products with orders (Admin only)
module.exports.getAllOrders = (getOrders) => {
	if(getOrders.isAdmin == true){
		return Product.find().then(result=>{
			const boughtProducts = result.filter(product => product.orders.length > 0);
			return(boughtProducts);	
		})
	}
	else{
		let message = Promise.resolve('No admin rights to access this feature.');
		return message.then((value) => {return value});
	}					
}


// GET ALL USERS INFO
module.exports.getAllUsers = (request, response) => {
	return Product.find().then(result => {
		const boughtProducts = result.map(product => product.orders);
		return result;
	})
}

















// ---------- for own-use functions ---------------
module.exports.deleteAllProducts = () => {
	return Product.deleteMany().then(result => {
		return result;
	})
}


// Reactivate Product (Admin only)
module.exports.reactivate = (productId, reStock) => {
	if(reStock.isAdmin == true){
		return Product.findOneAndUpdate(productId,
			{
				stocks: reStock.product.stocks
			}
		).then(result=>{
			let stocks = reStock.product.stocks;
			let newData = {
				stocks: stocks
			}
			if(reStock.product.stocks > 0){
				result.isActive = true;
				return result.save()
				.then(result =>{
					console.log(result);
					return result;
				})
				.catch(error => {
					console.log(error);
					return false;
				});
			}
			if(reStock.product.stocks == 0){
				result.isActive = false;
				return result.save()
				.then(result =>{
					console.log(result);
					return result;
				})
				.catch(error => {
					console.log(error);
					return false;
				});
			}
		})
	}
	else{
		let message = Promise.resolve('No admin rights to access this feature.');
		return message.then((value) => {return value});
	}
}