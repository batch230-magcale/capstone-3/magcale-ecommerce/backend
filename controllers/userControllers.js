const bcrypt = require("bcrypt");
const auth = require("../auth");

const User = require("../models/users");
const Product = require("../models/products");

// ------------------ s42 --------------------

module.exports.registerUser = (request,response) => {
	let newUser = new User({
		firstName: request.body.firstName,
		lastName: request.body.lastName,
		email: request.body.email,
		mobileNumber: request.body.mobileNumber,
		// userName: request.userName,
		password: bcrypt.hashSync(request.body.password, 15)
	});

	return User.findOne({$or: [{mobileNumber: request.body.mobileNumber}, {email: request.body.email}]})
    .then(result => {
        if(result != null && result.email == request.body.email){
            response.send (false);
        }
        else if(result != null && result.mobileNumber == request.body.mobileNumber){
            response.send (false);
        } 
		else{
			 // return false;
			return newUser.save().then((user, error) => {
				if(error){
					return error;
				}
				else{
					response.send (true);
				}
			})
			.catch(error =>{
				console.log(error);
				response.send(false);
			})
			
		}

	})
}


module.exports.loginUser = (request) => {
	return User.findOne({email: request.email}).then(result => {
		if(result == null){
			// return ("Email is incorrect!");
			return false;
		}
		else{
			const userPassword = bcrypt.compareSync(request.password, result.password);

			if(userPassword){
				return {access: auth.createAccessToken(result)};
			}
			else{
				// return ("Password is incorrect!");
				return false;
			}
		}
	})
}


// ------------------ s45 --------------------
// Non-admin User checkout (Create Order)

module.exports.userCheckout = (request, response) => {

	const userData = auth.decode(request.headers.authorization);
	Product.findById(request.body.productId)
		.then(async result => {
			if(userData.isAdmin == true){
				response.send ("Please login as guest user.");
			}

			else{
				let productName = await result.name;
				let price = await result.price;
				let totalAmount = await request.body.quantity * price;
				let stocks = await result.stocks;
				let quantity = await result.quantity;
				let isActive = await result.isActive;
				let reqQuantity = await request.body.quantity;

				let newData = {		
					userId: userData.id,
					userName: userData.userName,
					userEmail: userData.email,
					stocks:  stocks,
					isActive: isActive,
					quantity: request.body.quantity,
					productId: request.body.productId,
					totalAmount: totalAmount,
					products: [{		
						productId: request.body.productId,
						productName: productName,
						quantity: request.body.quantity,
						isActive: result.isActive
					}]
				};

				console.log(newData);

				let isProductUpdated = await Product.findById(newData.productId).then(products =>{
						products.orders.push({
							userId: newData.userId,
							userName: newData.userName,
							userEmail: newData.userEmail,
							quantity: newData.quantity,
							isActive: newData.isActive

						})
						if(products.stocks <= 0){			
							return ("No stocks available.");
						}
						else if(request.body.quantity > products.stocks){
							return("Not enough stocks.");
						}
						else if(request.body.quantity <= 0){
							return false;
						}
						else{
							products.stocks = products.stocks - newData.quantity;
							if(products.stocks == 0) {
							    products.isActive = false;
							}


							return products.save()
							.then(result =>{
								console.log(result);
								return true;
							})
							.catch(error => {
								console.log(error);
								return false;
							});
						}
					})
					console.log(isProductUpdated);

					let isUserUpdated = await User.findById(newData.userId)
					.then(users => {
						if(newData.stocks == 0){
							return ("No stocks available.");
						}
						else if(reqQuantity > stocks && stocks > 0){
							return ("Not enough stocks.");
						}
						else if(reqQuantity <= 0){
							return false;
						}
						else{
							users.orders.push({
								productName: newData.productName,
								quantity: newData.quantity,
								totalAmount: newData.totalAmount,
								products: newData.products
							});
						
							
							return users.save()
							.then(result => {
								console.log(result);
								return true;
							})
							.catch(error => {
								console.log(error);
								return false;
							})
						}
					})

					console.log(isUserUpdated);

					

							/*	
								(isUserUpdated == true && isProductUpdated == true)? response.send("User and Products are updated"): response.send("Not updated");
							*/
					
					if(isUserUpdated && isProductUpdated == true){
						response.send(true);
					}
					else if(isUserUpdated == false){
						response.send(false);
					}
					else if(isProductUpdated && isUserUpdated == "No stocks available."){
						response.send("No stocks available as of the moment.")
					}
					else if(isProductUpdated && isUserUpdated == "Not enough stocks."){
						response.send(false);
					}
					else if(isProductUpdated == false){
						response.send("Product was not updated.");
					}
					else if(isProductUpdated == "Product is Inactive."){
						response.send("Product is Inactive.");
					}
					else{
						response.send("User and Products were not updated");
					}
				
			}
		})
}



// Retrieve User Details
/*module.exports.getUserDetails = (userId) => {
	return User.findById(userId).then(result => {
		result.password = "**********";
		return result;
	})
}*/

module.exports.getProfile = (req, res) => {
	
	const userData = auth.decode(req.headers.authorization);

	console.log(userData);

	return User.findById(userData.id).then(result =>{
		result.password = "*********";
		res.send(result);
	})
}




//  ---------STRETCH GOAL ------------- (Unfinished)
/*
	- Set user as admin (Admin only)
	- Retrieve authenticated user’s orders
	- Retrieve all orders (Admin only)
	- Add to Cart (Highly suggested)
		- Added Products
		- Change product quantities
		- Remove products from cart
		- Subtotal for each item
		- Total price for all items

*/
// Set user as admin (Admin only)
module.exports.setAsAdmin = (userId, archived) => {
	if(archived.isAdmin == true){
		return User.findByIdAndUpdate(userId,
			{
				isAdmin: true
			}
		).then((result, error)=>{
			if(error){
				return false;
			}
			result.password = "********";
			return result;
		})
	}
	else{
		let message = Promise.resolve('No admin rights to access this feature.');
		return message.then((value) => {return value});
	}
}


// Set admin as guest (Admin only)
module.exports.setAsGuest = (userId, access) => {
	if(access.isAdmin == true){
		return User.findByIdAndUpdate(userId,
			{
				isAdmin: false
			}
		).then((result, error)=>{
			if(error){
				return false;
			}
			result.password = "********";
			return result;
		})
	}
	else{
		let message = Promise.resolve('No admin rights to access this feature.');
		return message.then((value) => {return value});
	}
}


// Retrieve authenticated user’s orders
module.exports.getUserOrders = (userId, token) => {
	if(token._id !== null){
		return User.findById(userId).then(result => {
			return result;
		}).then((result, error)=>{
			if(error){
				return false;
			}
			return result.orders.reverse();
		})
	}
	else{
		let message = Promise.resolve(false);
		return message.then((value) => {return value});
	}
}

// Retrieve all orders (Admin only)
module.exports.getUsersOrders = (usersOrders) => {
	if(usersOrders.isAdmin == true){
		return User.find().then(result => {
			const buyer = result.flatMap(user => user.orders);
			return buyer;
		})
	}
	else{
		let message = Promise.resolve('No admin rights to access this feature.');
		return message.then((value) => {return value});
	}
}

// Add to Cart
module.exports.addToCart = (request, response) => {

	const userData = auth.decode(request.headers.authorization);
	Product.findById(request.body.productId)
		.then(async result => {
			if(userData.isAdmin == true){
				response.send ("Please login as guest user.");
			}

			else{
				let productName = await result.name;
				let price = await result.price;
				let totalAmount = await request.body.quantity * price;
				let stocks = await result.stocks;
				let quantity = await result.quantity;
				let isActive = await result.isActive;
				let reqQuantity = await request.body.quantity;

				let newData = {		
					userId: userData.id,
					userName: userData.userName,
					userEmail: userData.email,
					stocks:  stocks,
					isActive: isActive,
					quantity: request.body.quantity,
					productId: request.body.productId,
					totalAmount: totalAmount,
					products: [{		
						productId: request.body.productId,
						productName: productName,
						quantity: request.body.quantity
					}]
				};

				console.log(newData);

				let isProductUpdated = await Product.findById(newData.productId).then(products =>{
						products.orders.push({
							userId: newData.userId,
							userName: newData.userName,
							userEmail: newData.userEmail,
							quantity: newData.quantity
						})
						if(products.stocks <= 0){			
							return ("No stocks available.");
						}
						else if(request.body.quantity > products.stocks){
							return("Not enough stocks.");
						}
						else{
							products.stocks = products.stocks - newData.quantity;
							if(products.stocks == 0) {
							    products.isActive = false;
							}
							else{
								console.log(products);
								return true
							}
						}
					})
					console.log(isProductUpdated);

					let isUserUpdated = await User.findById(newData.userId)
					.then(users => {
						if(newData.stocks == 0){
							return ("No stocks available.");
						}
						else if(reqQuantity > stocks && stocks > 0){
							return ("Not enough stocks.");
						}
						else{
							users.addToCart.push({
								productName: newData.productName,
								quantity: newData.quantity,
								totalAmount: newData.totalAmount,
								products: newData.products
							});
						
							
							return users.save()
							.then(result => {
								console.log(result);
								return true;
							})
							.catch(error => {
								console.log(error);
								return false;
							})
						}
					})

					console.log(isUserUpdated);

					

							/*	
								(isUserUpdated == true && isProductUpdated == true)? response.send("User and Products are updated"): response.send("Not updated");
							*/
					
					if(isUserUpdated && isProductUpdated == true){
						response.send(`Product Added to Cart.`);
					}
					else if(isUserUpdated == false){
						response.send(error);
					}
					else if(isProductUpdated && isUserUpdated == "No stocks available."){
						response.send("No stocks available as of the moment.")
					}
					else if(isProductUpdated && isUserUpdated == "Not enough stocks."){
						response.send(`Not enough stocks, there are only ${stocks} stock/s of ${productName} left.`);
					}
					else if(isProductUpdated == false){
						response.send("Product was not updated.");
					}
					else if(isProductUpdated == "Product is Inactive."){
						response.send("Product is Inactive.");
					}
					else{
						response.send("User and Products were not updated");
					}
				
			}
		})
}

// Change cart quantity
// module.exports.changeQuantityInCart = (req, res) => {
// 	const data = {
// 		user: request.body,
// 		isAdmin: auth.decode(request.headers.authorization).isAdmin,
// 		id: auth.decode(request.headers.authorization)._id
// 	}

// 	User.find({_id: data.id})
// 	.then(result => {
// 		const add = result.flatMap(product => {
			
// 		})
// 	})
// }














// ---------- for own-use functions ---------------

// GET ALL USERS INFO
module.exports.getAllUsers = () => {
	return User.find().then(result => {
		return result;
	})
}


// DELETE USER BY ID
module.exports.deleteUser = (userId) => {
	return User.findByIdAndDelete(userId).then(result => {
		return result;
	})
}

// DELETE ALL USERS
module.exports.deleteAllUsers = () => {
	return User.deleteMany().then(result => {
		return result;
	})
}


