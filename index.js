// Dependencies
const express = require("express");
const mongoose = require("mongoose");
const app = express();

const cors = require("cors");
const userRoute = require("./routes/userRoutes");
const productRoute = require("./routes/productRoutes");



// Middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.use("/users", userRoute);
app.use("/products", productRoute);



// Connecting to MongoDB database
mongoose.connect("mongodb+srv://admin:admin@batch230.hqlo3o4.mongodb.net/NewDatabase?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

mongoose.connection.once("open", () => console.log("Now connected to Magcale-Mongo DB Atlas"));

app.listen(process.env.PORT || 3002, () => {
	console.log(`API is now online on port ${process.env.PORT || 3002}`)
})