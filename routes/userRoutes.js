const mongoose = require("mongoose");
const express = require("express");
const router = express.Router();
const auth = require("../auth");

const userController = require("../controllers/userControllers");

// ------------------ s42 --------------------
// REGISTER USER
router.post("/register", userController.registerUser)


// LOGIN
router.post("/login", (request, response) => {
	userController.loginUser(request.body).then(resultFromController => response.send(resultFromController));
})




// ------------------ s45 --------------------
// Non-admin User checkout (Create Order)

router.post("/checkout", auth.verify, userController.userCheckout);




// Retrieve User Details
/*router.get("/:userId/userDetails", (request, response) => {
	userController.getUserDetails(request.params.userId).then(resultFromController => response.send(resultFromController));
})*/
router.get("/details", auth.verify, userController.getProfile);

//  ---------STRETCH GOAL ------------- (Unfinished)
/*
	- Set user as admin (Admin only)
	- Retrieve authenticated user’s orders
	- Retrieve all orders (Admin only)
	- Add to Cart (Highly suggested)
		- Added Products
		- Change product quantities
		- Remove products from cart
		- Subtotal for each item
		- Total price for all items

*/
// Set user as admin (Admin only)
router.patch("/:userId/setAsAdmin", auth.verify, (request,response) => 
	{
		const archived = {
			user: request.body,
			isAdmin: auth.decode(request.headers.authorization).isAdmin
			
		}

		userController.setAsAdmin(request.params.userId, archived).then(resultFromController => {
			response.send(resultFromController)
		})
})

// Set admin as guest (Admin only)
router.patch("/:userId/setAsGuest", auth.verify, (request,response) => 
	{
		const access = {
			user: request.body,
			isAdmin: auth.decode(request.headers.authorization).isAdmin
		}

		userController.setAsGuest(request.params.userId, access).then(resultFromController => {
			response.send(resultFromController)
		})
})


// Retrieve authenticated user’s orders
router.get("/:userId/getUserOrders", auth.verify, (request,response) => 
	{
		const token = {
			// user: request.params.userId,
			// isAdmin: auth.decode(request.headers.authorization).isAdmin,
			_id: auth.decode(request.headers.authorization)._id
		}

		userController.getUserOrders(request.params.userId, token).then(resultFromController => {
			response.send(resultFromController)
		})
})


// Retrieve all orders (Admin only)
router.get("/usersWithOrders", auth.verify, (request,response) => 
	{
		const usersOrders = {
			isAdmin: auth.decode(request.headers.authorization).isAdmin
		}
		userController.getUsersOrders(usersOrders).then(resultFromController => {
			response.send(resultFromController)
		})
})


// Add to Cart
router.post("/addToCart", auth.verify, userController.addToCart);





// ---------- for own-use functions ---------------

// GET ALL USERS INFO
router.get("/getAllUsers", (request, response) => {
	userController.getAllUsers().then(resultFromController => response.send(resultFromController));
})



// DELETE USER BY ID
router.delete("/:userId/delete", (request, response) => {
	userController.deleteUser(request.params.userId).then(resultFromController => response.send(resultFromController));
})


// DELETE ALL USERS
router.delete("/deleteAllUsers", (request, response) => {
	userController.deleteAllUsers().then(resultFromController => response.send(resultFromController));
})









module.exports = router;