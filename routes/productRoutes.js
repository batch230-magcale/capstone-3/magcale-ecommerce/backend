const express = require("express");
const router = express.Router();

const productController = require("../controllers/productControllers");
const auth = require("../auth");

// ------------------ s43 --------------------
// CREATE PRODUCT
router.post("/create", auth.verify, (request,response) => 
	{
		const result = {
			product: request.body,
			isAdmin: auth.decode(request.headers.authorization).isAdmin
		}

		productController.createProduct(request.body, result).then(resultFromController => {
			response.send(resultFromController)
		})
})

// RETRIEVE ALL ACTIVE PRODUCTS
router.get("/active", (request, response) => {
	productController.retrieveActiveProducts().then(resultFromController => response.send(resultFromController));
})

// Find Product by description
router.post('/search', productController.searchProduct);


// Get all product info
router.get("/all", (request, response) => {
	productController.getAllProducts().then(resultFromController => response.send(resultFromController));
})


// ------------------ s44 --------------------
// RETRIEVE SINGLE PRODUCT
router.get("/:productId", (request, response) => {
	productController.retrieveSingleProduct(request.params.productId).then(resultFromController => response.send(resultFromController));
})


// UPDATE PRODUCT INFORMATION (ADMIN)
router.patch("/:productId/update", auth.verify, (request,response) => 
	{
		const updatedInfo = {
			product: request.body, 	
			isAdmin: auth.decode(request.headers.authorization).isAdmin
		}

		productController.updateProductInformation(request.params.productId, updatedInfo).then(resultFromController => {
			response.send(resultFromController)
		})
})


// Archive Product (Admin only)
router.patch("/:productId/archive", auth.verify, (request,response) => 
	{
		const archived = {
			product: request.body,
			isAdmin: auth.decode(request.headers.authorization).isAdmin
		}

		productController.archiveProduct(request.params.productId, archived).then(resultFromController => {
			response.send(resultFromController)
		})
})


//  ---------STRETCH GOAL ------------- (Unfinished)
/*
	- Set user as admin (Admin only)
	- Retrieve authenticated user’s orders
	- Retrieve all orders (Admin only)
	- Add to Cart (Highly suggested)
		- Added Products
		- Change product quantities
		- Remove products from cart
		- Subtotal for each item
		- Total price for all items

*/
// Retrieve all products with orders (Admin only)
router.post("/productWithOrders", auth.verify, (request, response) => 
	{
		const getOrders = {
			isAdmin: auth.decode(request.headers.authorization).isAdmin
		}

		productController.getAllOrders(getOrders).then(resultFromController => response.send(resultFromController));
})










// ---------- for own-use functions ---------------
router.delete("/deleteAllProducts", (request, response) => {
	productController.deleteAllProducts().then(resultFromController => response.send(resultFromController));
})




// Reactivate Product (Admin only)
router.patch("/:productId/reactivate", auth.verify, (request,response) => 
	{
		const reStock = {
			product: request.body,
			isAdmin: auth.decode(request.headers.authorization).isAdmin
		}

		productController.reactivate(request.params.productId, reStock).then(resultFromController => {
			response.send(resultFromController)
		})
})




module.exports = router;