const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "First name is required."]
	},
	lastName: {
		type: String,
		required: [true, "Last name is required."]
	},
/*	userName: {
		type: String,
		required: [true, "Username is required."]
	},*/
	email:{
		type: String,
		required: [true, "Email is required."]
	},
	password: {
		type: String,
		required: [true, "Password is required."]
	},
	mobileNumber:{
		type: String,
		required: [true, "Mobile number is required."]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	addToCart: [{
		totalAmount: {
			type: Number,
			required: [true, "Total amount is required"]
		},
		purchasedOn: {
			type: Date,
			default: new Date()
		},
		products: [{
			productId: {
				type: String,
				required: [true, "Product ID is required."]
			},
			productName: {
				type: String,
				required: [true, "Product name is required"]
			},
			quantity: {
				type: Number,
				required: [true, "Quantity is required."]
			}
		}]
	}],
	orders: [{
		totalAmount: {
			type: Number,
			required: [true, "Total amount is required"]
		},
		purchasedOn: {
			type: Date,
			default: new Date()
		},
		products: [{
			productId: {
				type: String,
				required: [true, "Product ID is required."]
			},
			productName: {
				type: String,
				required: [true, "Product name is required"]
			},
			quantity: {
				type: Number,
				required: [true, "Quantity is required."]
			},
			isActive: {
				type: Boolean,
				required: [true, "Status is required after checkout."]
			}
		}]
	}]
});

module.exports = mongoose.model("User", userSchema);